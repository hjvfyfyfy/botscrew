package ua.botsCrew.testProject.view.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ua.botsCrew.testProject.view.command.Command;

public class СommandFactory {
	private final static Logger logger = Logger.getLogger(СommandFactory.class);

	private final Map<String, Command> commandMap;

	private СommandFactory(HashMap<String, Command> commandMap) {
		this.commandMap = commandMap;
	}

	public boolean executeCommand(String input) {
		logger.info("executing command [" + input + "]");
		boolean result = false;
		Set<String> keySet = commandMap.keySet();
		// searching needed command
		for (String command : keySet) {
			if (equelsCommand(command, input) == true) {
				String parameters = getParameters(command, input);
				// execute needed command from parameters
				commandMap.get(command).execute(parameters);
				result = true;
				break;
			}
		}
		return result;
	}

	private boolean equelsCommand(String command, String parameters) {
		Pattern p = Pattern.compile("^" + command);
		Matcher m = p.matcher(parameters);
		return m.find();
	}

	private String getParameters(String command, String input) {
		return input.substring(command.length()).trim();
	}

	public String listСommands() {
		logger.info("show list of commands");
		StringBuilder stringBuilder = new StringBuilder();
		if (commandMap.isEmpty()) {
			stringBuilder.append(" books list is empty");
		} else {
			for (String key : commandMap.keySet()) {
				stringBuilder.append(" " + key + " : " + commandMap.get(key).getDescription() + "\n");
			}
		}
		return stringBuilder.toString();
	}

	public static СommandFactory initialization(HashMap<String, Command> commandMap) {
		СommandFactory commandFactory = new СommandFactory(commandMap);
		return commandFactory;
	}
}
