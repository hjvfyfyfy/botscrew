package ua.botsCrew.testProject.view.command.impl;

import java.util.List;

import org.apache.log4j.Logger;

import ua.botsCrew.testProject.model.dao.DaoManager;
import ua.botsCrew.testProject.model.model.BookModel;
import ua.botsCrew.testProject.view.command.Command;

public class ShowAllBooksCommand implements Command{
	private final static Logger logger = Logger.getLogger(ShowAllBooksCommand.class);
	
	private DaoManager daoManager;
	private String name = "all books";
	private String description = "show all books from db";
	
	// command show all books from db
	public void execute(String parameters) {
		List<BookModel> booksList = daoManager.getBookDao().findAll();
		System.out.println(">>> our books:");
		for(BookModel book: booksList){
			System.out.println(" " + book);
		}
		logger.info("show all books");
	}

	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public void setDaoManager(DaoManager daoManager) {
		this.daoManager = daoManager;
	}
}
