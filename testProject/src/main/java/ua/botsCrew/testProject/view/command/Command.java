package ua.botsCrew.testProject.view.command;

import ua.botsCrew.testProject.model.dao.DaoManager;

public interface Command {
	public void execute(String parameters);
	public String getName();
	public String getDescription();
	public void setDaoManager(DaoManager daoManager);
}
