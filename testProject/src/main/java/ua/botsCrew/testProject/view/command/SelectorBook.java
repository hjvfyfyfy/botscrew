package ua.botsCrew.testProject.view.command;

import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import ua.botsCrew.testProject.model.model.BookModel;

public class SelectorBook {
	private final static Logger logger = Logger.getLogger(SelectorBook.class);
	private Integer number;
	
	public BookModel selectBookFromList(List<BookModel> listBooks) {
		while (true) {
			System.out.println(">>> we have few books with such name please choose one by typing a number of book:");
			System.out.print(printBooks(listBooks));
			System.out.print("> ");
			if (getNumber() && isNumberValid(listBooks)) {
				// break from cycle if number is valid
				break;
			} else {
				// otherwise out message
				System.out.println(">>> there is no book with this number");
			}
		}
		if (number == 0) {
			// if number "0" out from selecting book
			System.out.println(">>> close selector");
			return null;
		} else {
			return listBooks.get(number - 1);
		}
	}

	// out list of books with numbers
	private String printBooks(List<BookModel> listBooks) {
		StringBuilder books = new StringBuilder();
		books.append(" 0.  - exit\n");
		int i = 1;
		for (BookModel book : listBooks) {
			books.append(" " + i + ".  " + book + "\n");
			i++;
		}
		return books.toString();
	}

	// entered number of book, true if successfully
	private boolean getNumber() {
		String line = null;
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);;
		try{
			line = scanner.nextLine().trim();
			number = Integer.parseInt(line);
			logger.info("entered " + number);
		} catch (Exception e) {
			logger.error("line is not number");
			return false;
		}
		return true;
	}

	private boolean isNumberValid(List<BookModel> listBooks) {
		if(0 <= number && number <= listBooks.size()){
			return true;
		}
		logger.error("number is not valid");
		return false;
	}
}
