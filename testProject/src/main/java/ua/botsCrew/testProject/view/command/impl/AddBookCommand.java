package ua.botsCrew.testProject.view.command.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ua.botsCrew.testProject.model.dao.DaoManager;
import ua.botsCrew.testProject.model.model.BookModel;
import ua.botsCrew.testProject.view.command.Command;

public class AddBookCommand implements Command {
	private final static Logger logger = Logger.getLogger(AddBookCommand.class);

	private DaoManager daoManager;
	private String name = "add";
	private String description = "add book to db";
	
	// command add new book to db
	public void execute(String parameters) {
		String author = getAuthor(parameters);
		String name = getName(parameters);
		// create new book
		BookModel book = new BookModel();
		book.setAuthor(author);
		book.setName(name);
		
		daoManager.getBookDao().save(book);
		logger.info("add book [" + book + "]");
		System.out.println(">>> book " + book + " was added");
	}
	// parsing author of book from string
	private String getAuthor(String parameters){
		Pattern p = Pattern.compile("^([^\"”“]+)(\"|“)");
		Matcher m = p.matcher(parameters);
		while (m.find()) {
			return m.group(1).trim();
		}
		// if there are no name of author, set [unknown]
		return "[unknown]";
	}
	// parsing name of book from string
	private String getName(String parameters){
		Pattern p = Pattern.compile("(\"|“)([^\"”“]+)(\"|”)");
		Matcher m = p.matcher(parameters);
		while (m.find()) {
		  return m.group(2).trim();
		}
		return "[unknown]";
	}
	
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public void setDaoManager(DaoManager daoManager) {
		this.daoManager = daoManager;
	}
}
