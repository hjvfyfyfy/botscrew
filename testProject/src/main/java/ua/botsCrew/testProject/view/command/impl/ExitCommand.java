package ua.botsCrew.testProject.view.command.impl;

import org.apache.log4j.Logger;

import ua.botsCrew.testProject.model.dao.DaoManager;
import ua.botsCrew.testProject.view.command.Command;

public class ExitCommand implements Command {
	private final static Logger logger = Logger.getLogger(ExitCommand.class);

	private DaoManager daoManager;
	private String name = "exit";
	private String description = "exit from program";

	// command close the program
	public void execute(String parameters) {
		daoManager.closeSession();
		logger.info("close the program");
		System.exit(0);
	}

	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public void setDaoManager(DaoManager daoManager) {
		this.daoManager = daoManager;
	}
}
