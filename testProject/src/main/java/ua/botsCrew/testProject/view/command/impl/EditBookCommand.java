package ua.botsCrew.testProject.view.command.impl;

import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import ua.botsCrew.testProject.model.dao.DaoManager;
import ua.botsCrew.testProject.model.model.BookModel;
import ua.botsCrew.testProject.view.command.Command;
import ua.botsCrew.testProject.view.command.SelectorBook;

public class EditBookCommand implements Command {
	private final static Logger logger = Logger.getLogger(EditBookCommand.class);

	private DaoManager daoManager;
	private String name = "edit";
	private String description = "edit book from db";
	private SelectorBook selectorBook;

	// command edit new from db
	public void execute(String parameters) {
		List<BookModel> listBooks = daoManager.getBookDao().findByName(parameters);
		if (listBooks.size() == 0) {
			// if there is no book 
			logger.info("failed to remove book [" + parameters + "]");
			System.out.println(">>> failed to remove \"" + parameters + "\"");
		} else {
			BookModel book;
			if (listBooks.size() == 1) {
				// if there is only one found book 
				book = listBooks.get(0);
			} else {
				// selection of books from found list of books
				book = selectorBook.selectBookFromList(listBooks);
			}
			if (book != null) {
				updateNameOfBook(book);
				daoManager.getBookDao().save(book);
				logger.info("update [" + book + "]");
				System.out.println(">>> book " + book + " was updated");
			} else {
				logger.info("close menu item [" + name + "]");
			}
		}
	}
	private void updateNameOfBook(BookModel book){
		System.out.println(">>> enter new name :");
		System.out.print("> ");
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		String editName = scanner.nextLine().trim();
		book.setName(editName);
	}
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setDaoManager(DaoManager daoManager) {
		this.daoManager = daoManager;
	}

	public void setSelectorBook(SelectorBook selectorBook) {
		this.selectorBook = selectorBook;
	}
}
