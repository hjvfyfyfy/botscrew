package ua.botsCrew.testProject.view.command.impl;

import java.util.List;

import org.apache.log4j.Logger;

import ua.botsCrew.testProject.model.dao.DaoManager;
import ua.botsCrew.testProject.model.model.BookModel;
import ua.botsCrew.testProject.view.command.Command;
import ua.botsCrew.testProject.view.command.SelectorBook;

public class RemoveBookCommand implements Command {
	private final static Logger logger = Logger.getLogger(RemoveBookCommand.class);

	private DaoManager daoManager;
	private String name = "remove";
	private String description = "remove book from db";
	private SelectorBook selectorBook;
	
	// command delete some book from db
	public void execute(String parameters) {
		List<BookModel> listBooks = daoManager.getBookDao().findByName(parameters);
		if(listBooks.size() == 0){
			logger.info("failed to remove book [" + parameters + "]");
			System.out.println(">>> failed to remove \"" + parameters + "\"");
		} else {
			BookModel book;
			if(listBooks.size() == 1){
				// if there is only one found book 
				book = listBooks.get(0);
			} else {
				// selection of books from found list of books
				book = selectorBook.selectBookFromList(listBooks);
			}
			if(book != null){
				daoManager.getBookDao().delete(book);
				logger.info("remove [" + book + "]");
				System.out.println(">>> book " + book + " was removed");
			} else {
				logger.info("close menu item [" + name + "]");
			}
		}
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setDaoManager(DaoManager daoManager) {
		this.daoManager = daoManager;
	}

	public void setSelectorBook(SelectorBook selectorBook) {
		this.selectorBook = selectorBook;
	}
}
