package ua.botsCrew.testProject.view;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import ua.botsCrew.testProject.view.command.СommandFactory;

public class Menu {
	private final static Logger logger = Logger.getLogger(Menu.class);

	private СommandFactory commandFactory;

	public Menu(СommandFactory commandFactory) {
		super();
		this.commandFactory = commandFactory;
		logger.info("Was created class Menu");
	}

	public void start() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println(">>> Input coomand (help showing all command):");
		String inLine;
		while (true) {
			System.out.print("> ");
			inLine = scanner.nextLine().trim();
			if (isCommandHelp(inLine)) {
				// if command "help", show list from commands
				System.out.print(commandFactory.listСommands());
				logger.info("Ipnut [" + inLine + "]");
			} else if (commandFactory.executeCommand(inLine) == false) {
				// "executeCommand()" return false if there are no searching command 
				logger.error("Ipnut error [" + inLine + "] is not valid");
				System.out.println("[" + inLine + "] is not recognized as command");
			} else {
				logger.info("Ipnut [" + inLine + "]");
			}
		}
	}

	private boolean isCommandHelp(String input) {
		Pattern p = Pattern.compile("^help$");
		Matcher m = p.matcher(input);
		return m.matches();
	}
}
