package ua.botsCrew.testProject.model.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ua.botsCrew.testProject.model.dao.BookDao;
import ua.botsCrew.testProject.model.model.BookModel;

public class BookDaoImpl implements BookDao{
	
	private Session session;
	private final String HQL_find_all = "FROM BookModel";
	private final String HQL_find_by_name = "FROM BookModel WHERE name like :name";
	private final String HQL_find_by_author = "FROM BookModel author like :author";
	private final String HQL_find_by_id = "FROM BookModel WHERE id = :id";
	
	public List<BookModel> findAll() {
		return session.createQuery(HQL_find_all, BookModel.class).getResultList();
	}

	public List<BookModel> findByName(String name) {
		return session.createQuery(HQL_find_by_name, BookModel.class).setParameter("name", name).getResultList();
	}

	public List<BookModel> findByAuthor(String author) {
		return session.createQuery(HQL_find_by_author, BookModel.class).setParameter("author", author).getResultList();
	}

	public BookModel findById(int id) {
		return session.createQuery(HQL_find_by_id, BookModel.class).setParameter("id", id).getSingleResult();
	}

	public BookModel save(BookModel book) {
		Transaction tr = session.beginTransaction();
		session.saveOrUpdate(book);
		tr.commit();
		return book;
	}

	public void delete(BookModel book) {
		Transaction tr = session.beginTransaction();
		session.delete(book);
		tr.commit();
	}

	public void setSession(Session session) {
		this.session = session;
	}
}
