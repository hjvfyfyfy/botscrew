package ua.botsCrew.testProject.model.dao;

import java.util.List;

import ua.botsCrew.testProject.model.model.BookModel;

public interface BookDao {
	
	List<BookModel> findAll();
	List<BookModel> findByName(String name);
	List<BookModel> findByAuthor(String author);
	BookModel findById(int id);
	
	BookModel save(BookModel book);
	void delete(BookModel book); 
}
