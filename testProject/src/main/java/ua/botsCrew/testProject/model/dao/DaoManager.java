package ua.botsCrew.testProject.model.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;

// class save all DAO
public class DaoManager {
	private final static Logger logger = Logger.getLogger(DaoManager.class);
	private Session session;
	private BookDao bookDao;
	
	public DaoManager(BookDao bookDao) {
		super();
		this.bookDao = bookDao;
	}
	
	public void setSession(Session session) {
		this.session = session;
	}

	public void closeSession(){
		logger.info("close session");
		session.close();
	}
	
	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	public BookDao getBookDao() {
		return bookDao;
    }
}
