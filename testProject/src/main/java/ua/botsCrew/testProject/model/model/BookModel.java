package ua.botsCrew.testProject.model.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BookModel implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String author;
	private Date updatingDate;
	
	public BookModel() {
		super();
	}
	public BookModel(int id, String name, String author, Date updatingDate) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
		this.updatingDate = updatingDate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getUpdatingDate() {
		return updatingDate;
	}
	public void setUpdatingDate(Date updatingDate) {
		this.updatingDate = updatingDate;
	}
	@Override
	public String toString() {
		if(updatingDate != null){
			SimpleDateFormat dt1 = new SimpleDateFormat("yy-mm-dd hh:mm");
			return "- " + author + " " + "\"" + name + "\"" + "\tD:" + dt1.format(updatingDate);
		} else {
			return "- " + author + " " + "\"" + name + "\"";
		}
	}
}
