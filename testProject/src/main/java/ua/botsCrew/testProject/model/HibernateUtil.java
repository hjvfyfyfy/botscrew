package ua.botsCrew.testProject.model;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private final static Logger logger = Logger.getLogger(HibernateUtil.class);
	private static final Session session = createSession();

	private static Session createSession(){
		logger.info("open session");
		return buildSessionFactory().openSession();
	}
	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static Session getSession() {
		return session;
	}

	public static void close() {
		// Close caches and connection pools
		session.close();
	}
}
