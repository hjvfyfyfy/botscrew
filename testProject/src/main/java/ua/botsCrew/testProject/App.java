package ua.botsCrew.testProject;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ua.botsCrew.testProject.view.Menu;

public class App {
	private final static Logger logger = Logger.getLogger(App.class);
	static{
		// logger configuration from file log4j.xml
		DOMConfigurator.configure("src/resource/log4j.xml");
	}
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-beans.xml");
		Menu menu = (Menu) context.getBean("menu");
		logger.info("start program");
		menu.start();
	}
}
